﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

using Multipart.StreamFiles;

namespace File.Api.Controllers {
    using File = System.IO.File;

    [Route("api")]
    public class UploadController : Controller {

        readonly string _dir;
        public UploadController() {
            _dir = Path.Combine(Directory.GetCurrentDirectory(), "tmp");
        }

        [HttpPost("base64")]
        public async Task<IActionResult> base64(string base64) {
            var path = Path.Combine(_dir, Path.GetRandomFileName());

            await System.IO.File.WriteAllBytesAsync(path, Convert.FromBase64String(base64));
            return Ok(new { path });
        }

        [HttpPost("upload")]
        public async Task<IActionResult> Upload(List<IFormFile> files) {
            long size = files.Sum(f => f.Length);

            var paths = new List<string>();

            foreach (var formFile in files)
                if (formFile.Length > 0) {
                    var file = Path.Combine(_dir, Path.GetRandomFileName());
                    paths.Add(file);
                    using(var stream = new FileStream(file, FileMode.Create))
                    await formFile.CopyToAsync(stream);
                }

            return Ok(new { count = files.Count, size, paths });
        }

        class TestModel {
            public string Name { get; set; }
            public string Description { get; set; }
            public IEnumerable<FileInfo> Files { get; set; }
        }
        class TestModelStream {
            public string Name { get; set; }
            public string Description { get; set; }
            public DisposableCollection<Stream> Files { get; set; }
        }

        enum StreamOptions {
            File,
            Stream,
            FileStream
        }

        [HttpPost("streamfiles")]
        [DisableModelBinding]
        public async Task<IActionResult> ControllerModelStream() {
            Debug.WriteLine("controller");
            System.Diagnostics.Trace.TraceInformation("controllermodelstream");
            List<string> filepaths = new List<string>();
            var model = await this.StreamFilesModel<TestModel>();

            return Ok(new { Model = model, Files = model.Files });
        }

        [HttpPost("streamoriginal")]
        [DisableModelBinding]
        public async Task<IActionResult> OriginalStream() {
            FormValueProvider formModel;
            var dir = Path.Combine(Directory.GetCurrentDirectory(), "tmp");
            var paths = new List<string>();
            var file = Path.Combine(dir, Path.GetRandomFileName());
            using(var stream = System.IO.File.Create(file))
            formModel = await Request.StreamFile(stream);

            var viewModel = new TestModel();
            var bindingSuccessful = await TryUpdateModelAsync(viewModel, prefix: "", valueProvider : formModel);

            if (!bindingSuccessful && !ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(viewModel);
        }

        // GET api/values
        [HttpGet]
        public string Get() {
            return "upload vs stream test";
        }
    }

}
