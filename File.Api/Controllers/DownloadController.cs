﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

using Multipart.StreamFiles;

namespace File.Api.Controllers {
    using File = System.IO.File;

    [Route("api")]
    public class DownloadController : Controller {

        readonly string _file;
        readonly string _contenttype;
        readonly byte[] _bytes;
        readonly FileStream _fileStream;
        readonly MemoryStream _memStream;

        public DownloadController() {
            _file = Path.Combine(Directory.GetCurrentDirectory(), "data", "sml.jpg");
            _contenttype = "application/octet-stream";
            _bytes = System.IO.File.ReadAllBytes(_file);
            _fileStream = new FileStream(_file, FileMode.Open, FileAccess.Read);
            _memStream = new MemoryStream(_bytes);
        }

        [HttpGet("base64")]
        public async Task<IActionResult> Base64() {
            var base64 = Convert.ToBase64String(await System.IO.File.ReadAllBytesAsync(_file));
            return Ok(base64);
        }

        [HttpGet("base64/mem")]
        public IActionResult Base64mem() {
            var base64 = Convert.ToBase64String(_bytes);
            return Ok(base64);
        }

        [HttpGet("download")]
        public IActionResult Download() {
            return File(_file, _contenttype);
        }

        [HttpGet("download/mem")]
        public IActionResult DownloadMem() {
            return File(_bytes, _contenttype);
        }

        [HttpGet("stream")]
        public IActionResult Stream() {
            return new FileStreamResult(_fileStream, _contenttype);
        }

        [HttpGet("stream/mem")]
        public IActionResult StreamMem() {
            return new FileStreamResult(_memStream, _contenttype);
        }

    }

}
