﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Net.Http.Headers;

namespace Multipart.StreamFiles {
    public static class ControllerExtensions {
        // public static Task<DisposableCollection<Stream>> StreamFiles(this ControllerBase controller) {

        //     FormValueProvider formModel;
        //     var dir = Path.Combine(Directory.GetCurrentDirectory(), "tmp");
        //     var paths = new List<string>();
        //     var file = Path.Combine(dir, Path.GetRandomFileName());
        //     using(var stream = System.IO.File.Create(file))
        //     formModel = await Request.StreamFile(stream);

        //     var viewModel = new TestModel();
        //     var bindingSuccessful = await TryUpdateModelAsync(viewModel, prefix: "", valueProvider : formModel);

        //     if (!bindingSuccessful && !ModelState.IsValid)

        //         throw new Exception();
        // }
        // public static Task<DisposableCollection<Stream>> StreamFiles<T>(this ControllerBase controller, out T model) where T : class, new() {

        //     FormValueProvider formModel;
        //     var dir = Path.Combine(Directory.GetCurrentDirectory(), "tmp");
        //     var paths = new List<string>();
        //     var file = Path.Combine(dir, Path.GetRandomFileName());
        //     using(var stream = System.IO.File.Create(file))
        //     formModel = await Request.StreamFile(stream);

        //     model = UpdateModel(form, controller);
        // }

        public static Task<IEnumerable<FileInfo>> StreamToFile(this ControllerBase controller, string directory = null) {
            throw new Exception();
        }
        public static Task<IEnumerable<FileInfo>> StreamToFile<T>(this ControllerBase controller, out T model, string directory = null) where T : class, new() {
            throw new Exception();
        }

        public static Task<IEnumerable<IFormFile>> StreamFormFiles(this ControllerBase controller) {
            throw new Exception();
            //var files = await controller.Request.StreamFiles();
        }
        public static async Task<T> StreamFilesModel<T>(this ControllerBase controller, string directory = null) where T : class, new() {
            var fileModel = await controller.Request.StreamFilesModel();
            var model = (T)(await UpdateModel<T>(fileModel.Item2, controller));
            Debug.WriteLine($"-- file:{fileModel.Item1} model:{fileModel.Item2}");
            Trace.TraceWarning("tracewarn");
            return model;
        }

        static async Task<T> UpdateModel<T>(FormValueProvider form, ControllerBase controller) where T : class, new() {
            var model = new T();
            var bindingSuccessful = await controller.TryUpdateModelAsync<T>(model, prefix: "", valueProvider : form);
            if (!bindingSuccessful)
                controller.ModelState.AddModelError("binding", "Error binding model");

            controller.TryValidateModel(model);
            return model;
        }
    }
}
