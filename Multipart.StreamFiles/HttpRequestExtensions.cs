﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Net.Http.Headers;

namespace Multipart.StreamFiles {
    public static class HttpRequestExtensions {
        static readonly FormOptions _defaultFormOptions = new FormOptions();

        public static async Task<(IEnumerable<IFormFile>, FormValueProvider)> StreamFilesModel(this HttpRequest request) {
            if (!MultipartRequestHelper.IsMultipartContentType(request.ContentType))
                throw new Exception($"Expected a multipart request, but got {request.ContentType}");

            // Used to accumulate all the form url encoded key value pairs in the request.
            var formAccumulator = new KeyValueAccumulator();
            //var files = new List<IFormFile>();
            var files = new FormFileCollection();

            var boundary = MultipartRequestHelper.GetBoundary(MediaTypeHeaderValue.Parse(request.ContentType), _defaultFormOptions.MultipartBoundaryLengthLimit);
            var reader = new MultipartReader(boundary, request.Body);

            var section = await reader.ReadNextSectionAsync();
            while (section != null) {
                ContentDispositionHeaderValue contentDisposition;
                var hasContentDispositionHeader = ContentDispositionHeaderValue.TryParse(section.ContentDisposition, out contentDisposition);

                if (hasContentDispositionHeader) {
                    if (MultipartRequestHelper.HasFileContentDisposition(contentDisposition)) {
                        FileMultipartSection fileSection = section.AsFileSection();
                        var file = new FormFile(fileSection.FileStream, 0, fileSection.FileStream.Length, fileSection.Name, fileSection.FileName);
                        files.Add(file);
                        Debug.WriteLine($"name:{file.Name} filename:{file.FileName} length:{file.Length}");
                        // string filePath = Path.Combine(targetFilePath,"NEW-" + currentFile.FileName);

                        // using (var targetStream = File.Create(filePath))
                        //     await section.Body.CopyToAsync(targetStream).ConfigureAwait(false);

                        //await section.Body.CopyToAsync(targetStream);
                    } else if (MultipartRequestHelper.HasFormDataContentDisposition(contentDisposition)) {
                        // Content-Disposition: form-data; name="key"
                        // Do not limit the key name length here because the multipart headers length limit is already in effect.
                        var key = HeaderUtilities.RemoveQuotes(contentDisposition.Name);
                        var encoding = section.GetEncoding();
                        using(var streamReader = new StreamReader(section.Body, encoding, detectEncodingFromByteOrderMarks : true, bufferSize : 1024, leaveOpen : true)) {
                            // The value length limit is enforced by MultipartBodyLengthLimit
                            var value = await streamReader.ReadToEndAsync();
                            if (String.Equals(value, "undefined", StringComparison.OrdinalIgnoreCase))
                                value = String.Empty;

                            formAccumulator.Append(key.Value, value);

                            if (formAccumulator.ValueCount > _defaultFormOptions.ValueCountLimit)
                                throw new InvalidDataException($"Form key count limit {_defaultFormOptions.ValueCountLimit} exceeded.");
                        }
                    }
                }

                // Drains any remaining section body that has not been consumed and reads the headers for the next section.
                section = await reader.ReadNextSectionAsync();
            }
            // Bind form data to a model
            var formValueProvider = new FormValueProvider(BindingSource.Form, new FormCollection(formAccumulator.GetResults(), files), CultureInfo.CurrentCulture);

            return (files, formValueProvider);
        }
    }
}
