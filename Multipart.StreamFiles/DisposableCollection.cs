﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Multipart.StreamFiles {
    public class DisposableCollection<T> : Collection<T>, IDisposable where T : IDisposable {

        public void Dispose () {
            foreach (T obj in this) {
                obj.Dispose ();
            }
        }
    }
}
