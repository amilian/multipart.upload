﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Multipart.StreamFiles {
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class DisableModelBindingAttribute : Attribute, IResourceFilter {
        public void OnResourceExecuting(ResourceExecutingContext context) {
            var formValueProviderFactory = context.ValueProviderFactories
                .OfType<FormValueProviderFactory>()
                .FirstOrDefault();
            if (formValueProviderFactory != null)
                context.ValueProviderFactories.Remove(formValueProviderFactory);

            var jqueryFormValueProviderFactory = context.ValueProviderFactories
                .OfType<JQueryFormValueProviderFactory>()
                .FirstOrDefault();
            if (jqueryFormValueProviderFactory != null)
                context.ValueProviderFactories.Remove(jqueryFormValueProviderFactory);
        }

        public void OnResourceExecuted(ResourceExecutedContext context) {}
    }
}
